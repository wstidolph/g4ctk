@wip
Feature:
  DROPPING THIS FEATURE per MarkD
  Duplicate the v0.5 compliance tests, using a data table to make it extensible.
  (v0.5 is the first item in each table.)

  referencesets: searches, then fetches that same reference set by ID

  Scenario Outline: referencesets
    When I search for a <referenceSet>  with <accessionId> and <objectName>
    Then I get back an object with <id>, <md5checksum>, <ncbiTaxonId>, <description>, <assemblyId>, <sourceId>, <sourceAccessions>, <isDerived>
    And the same object can be fetched using GET with the <id>

    Examples:
    |referenceSet | accessionId | objectName | id | md5checksum | ncbiTaxonId | description | assemblyId | sourceId | sourceAccessions | isDerived |

