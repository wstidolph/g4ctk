Feature: The server should be able to come to life and shut down.

  @serverUp
  Scenario: A server is available
    Given a server exists
    When the server health is checked
    Then the server should respond with OK

  Scenario: A running server shuts down
    Given a running server
    When the server receives a kill signal
    Then the server exits cleanly