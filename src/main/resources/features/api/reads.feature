Feature: Execute the reads API

  Scenario Outline: Search Reads
    Given I have a result for a search on a "<datasetId>" and "<name>"
    Then the returned object should match "<name>"
    Examples:
    | datasetId | name |
    | ''        | blankSet |
    | '2'       | twoSet   |
