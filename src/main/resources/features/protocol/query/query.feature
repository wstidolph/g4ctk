Feature: The server has a query interface to fetch genomic data
  Scenario Outline: Repeat the existing conformance queries

    When I POST to URL <url>
    Then I should get a <status> response

    Examples:
      | url | status |
      | ""  | 200    |
