package org.ga4gh.ctk;

/**
 * Created by Wayne Stidolph on 5/15/2015.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

@SpringBootApplication
@EnableConfigServer
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
        String [] cukeOptions = {};
        try {
            byte exitstatus = cucumber.api.cli.Main.run(cukeOptions, Thread.currentThread().getContextClassLoader());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}