package org.ga4gh.ctk;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * Created by Wayne Stidolph on 5/12/2015.
 */
public class TestHooks {

    // here's an example 'hook' structure, runs only when .feature scenario has @serverUp tag
    @Before("@serverUp")
    public void ensureServer() {
        System.out.println("***** ensuring server up");
    }

    @After
    public void afterScenario(Scenario scenario){
        System.out.println("*********** finished scenario " + scenario.getName() + " result: " + scenario.getStatus());
    }
}
