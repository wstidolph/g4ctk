package org.ga4gh.ctk;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Wayne Stidolph on 5/12/2015.
 */
public class DemotestStepDefs {

    int numBananas;
    int priceBananas;

    @Given("^the price of a banana is (\\d+)c$")
    public void the_price_of_a_banana_is_c(int priceBananas) throws Throwable {
        // Express the Regexp above with the code you wish you had
        this.priceBananas = priceBananas;
    }

    @When("^I checkout (\\d+) banana$")
    public void I_checkout_banana(int numBananas) throws Throwable {
        // Express the Regexp above with the code you wish you had
        this.numBananas = numBananas;
    }

    @Then("^the total price should be (\\d+)c$")
    public void the_total_price_should_be_c(int expectedTotal) throws Throwable {
        // Express the Regexp above with the code you wish you had
        assertEquals("total should be num times price", expectedTotal, numBananas * priceBananas);
    }


}
