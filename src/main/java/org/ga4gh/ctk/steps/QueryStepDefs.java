package org.ga4gh.ctk.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.When;

/**
 * Created by Wayne Stidolph on 5/12/2015.
 */
public class QueryStepDefs {
    @When("^I POST to URL <url>$")
    public void I_POST_to_URL_url() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }
}
