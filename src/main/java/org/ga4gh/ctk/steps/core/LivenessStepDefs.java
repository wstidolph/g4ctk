package org.ga4gh.ctk.steps.core;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by Wayne Stidolph on 5/12/2015.
 */
public class LivenessStepDefs {
    @Given("^a server exists$")
    public void a_server_exists() throws Throwable {
        // Express the Regexp above with the code you wish you had

        assertThat("foo", equalTo("foo"));
        //throw new PendingException();
    }

    @When("^the server health is checked$")
    public void a_server_is_launched() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the server should respond with OK$")
    public void the_server_should_respond_with_OK() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^a running server$")
    public void a_running_server() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^the server receives a kill signal$")
    public void the_server_receives_a_kill_signal() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the server exits cleanly$")
    public void the_server_exits_cleanly() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

}
