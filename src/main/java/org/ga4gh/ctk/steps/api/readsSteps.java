package org.ga4gh.ctk.steps.api;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/**
 * Created by Wayne Stidolph on 5/14/2015.
 */
public class readsSteps {

    @Given("^I have a result for a search on a \"([^\"]*)\" and \"([^\"]*)\"$")
    public void I_have_a_result_for_a_search_on_a_and(String datasetid, String datasetName) throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @Then("^the returned object should match \"([^\"]*)\"$")
    public void the_returned_object_should_match(String expName) throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }
}
