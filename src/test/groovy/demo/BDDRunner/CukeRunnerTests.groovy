package demo.BDDRunner

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith
/**
 * Created by Wayne Stidolph on 5/4/2015.
 * Supplies config to Cucumber and gets Cucumber invoked.
 * Named *Tests so it's picked up by the normal JUnit runs, but then @RunWith() to
 * invoke the Cucumber BDD runner.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        format = ['pretty','html:build/cucumber.html','json:build/cucumber.json','junit:build/cucumber.xml'],
        glue = ['src/test/groovy'],
        features = ['src/test/resources/features']
)
public class CukeRunnerTests {}