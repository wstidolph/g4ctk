package demo
/**
 * Created by Wayne Stidolph on 5/4/2015.
 */
//@ContextConfiguration(classes = DemoApplication.class, loader = SpringApplicationContextLoader.class)
//@RunWith(Cucumber.class)

this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

    def bananaPrice = 0
    def numBananas = 0

    Given(~/^the price of a banana is (\d+)c$/) { int bprice ->
        // Write code here that turns the phrase above into concrete actions
        bananaPrice = bprice
    }


    When(~/^I checkout (\d+) banana$/) { int bnum ->
        // Write code here that turns the phrase above into concrete actions
        numBananas = bnum
    }

    Then(~/^the total price should be (\d+)c$/) { int price ->
        // Write code here that turns the phrase above into concrete actions
        (bananaPrice * numBananas) == price
    }