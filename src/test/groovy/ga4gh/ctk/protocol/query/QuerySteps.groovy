import groovyx.net.http.HTTPBuilder

/**
 * Created by Wayne Stidolph on 5/10/2015.
 */
this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

def docUrlPrefix = 'http://ga4gh.org/documentation/features.api/v0.5/ga4gh_api.html#/schema/';
def URLBASE = "http://demo.restfuse.com"
def http = new HTTPBuilder()

When(~/^I POST to URL "(.*?)"$/) { String url ->
    // Write code here that turns the phrase above into concrete actions
    String finalUrl = URLBASE + url


    print "trying URL: $url" // throw new PendingException()
}

Then(~/^I should get a (\d+) response$/) { int response ->
    // Write code here that turns the phrase above into concrete actions
    print "looking for $response" // throw new PendingException()
}