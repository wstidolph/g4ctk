package ga4gh.ctk.core

import cucumber.api.PendingException

/**
 * Created by Wayne Stidolph on 5/10/2015.
 */
this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)


Given(~/^no server exists$/) { ->
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException()
}

When(~/^a server is launched$/) { ->
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException()
}

Then(~/^the server should respond with OK$/) { ->
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException()
}

Given(~/^a running server$/) { ->
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException()
}

When(~/^the server receives a kill signal$/) { ->
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException()
}

Then(~/^the server exits cleanly$/) { ->
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException()
}