package g4ctk.ctk;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Wayne Stidolph on 5/12/2015.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        format ={"pretty", "html:build/cucumber.html", "json:build/cucumber.json", "junit:build/cucumber.xml"},
        features ="src/main/resources/features",
        glue = "ga4gh.ctk.steps"
)
public class CukesRunnerTest {
}
